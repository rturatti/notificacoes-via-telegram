# Notificações via Telegram com Lambda

Este script é para ser utilizado com lambda e um tópico SNS para envie de notificações para um usuário ou para um grupo no Telegram.

## Como configurar

Primeiro precisamos criar um BOT no telegram, para isto utilizaremos um bot chamado @botfather basta buscar por seu username no Telegram para encontra-lo e após abrir sua conversa clique em "COMEÇAR" ou mande o comando "/start" no seu chat caso o botão não seja apresentado.
![Image1](https://i.imgur.com/ZoMhEcX.png)

Agora vamos criar um novo bot, para isto envie o comando "/newbot" e preencha as informações como nome e usuário do bot conforme o bot vai lhe solicitando e no fim ele irá lhe passar o token do seu BOT.
![Image2](https://i.imgur.com/eluRmJV.png)

Agora precisaremos pegar o ID do seu usuário ou do seu grupo para o BOT enviar as mensagens, pasta isto acesse a URL abaixo lembrando de alterar o <TOKEN> pelo token do teu BOT.
```
https://api.telegram.org/bot<TOKEN>/getUpdates
```

Feito isto, se você deseja que o BOT envie as mensagens somente para seu usuário basta você iniciar uma conversa com o BOT e enviar uma mensagem qualquer para ele apenas para coletarmos o seu ID, já se você deseja que as mensagens vão para um grupo no Telegram será preciso que você crie o grupo e coloque o BOT como membro, e então envie a mensagem dentro do grupo feito isto acesse a URL novamente, a URL que antes estava vazia agora deve estar com um JSON.
![Image3](https://i.imgur.com/JxKVI6c.png)

Salve estas informações pois usaremos posteriormente, agora vamos para o console da AWS e vamos criar um tópico SNS, para isto siga este caminho: 
```
AWS Management Console > Simple Notification Service > Topics > Create topic
```
Feito isto coloque um nome e clique para criar o tópico.
![Image4](https://i.imgur.com/ZcTZHIf.png)

Agora vamos criar a função lambda, para isto siga este caminho:
```
AWS Management Console > Lambda > Create function
```
Feito isto coloque um nome para a função e selecione que utilizaremos a linguagem Python 3.7 e então clique para criar a função.
![Image6](https://i.imgur.com/VfMuYot.png)

Agora dentro da sua função Lambda desça até a parte de "Environment variables" e clique em "Manage environment variables".
![Image6](https://i.imgur.com/AKOyYRL.png)

Então clique em "Add environment variable" 2 vezes para adicionar 2 variaveis de ambiente.
![Image7](https://i.imgur.com/qaTwueJ.png)

As váriaveis que iremos criar são TOKEN e USER_ID que são os valores que coletamos anteriormente o token do BOT e o ID do usuário ou do grupo, criadas as váriaveis clique para salvar.
![Image8](https://i.imgur.com/v5JVjeM.png)

Agora subindo novamente para o topo da pagina do seu lambda coloque o código disponibilizado no arquivo lambda.py, feito isto clique para salvar sua função Lambda.
![Image9](https://i.imgur.com/C4go9Pm.png)

Feito isto agora vamos adicionar o gatilho do tópico SNS, para isto vá até o topo do seu Lambda e clique em "Add trigger".
![Image10](https://i.imgur.com/zPIlm2O.png)

Agora na proxima tela no campo "Select a trigger" selecione o SNS.
![Image11](https://i.imgur.com/qnycdjF.png)

Feito isto agora no campo "SNS Topic" selecione o seu tópico do SNS previamente criado e clique em "Add".
![Image12](https://i.imgur.com/amizZGg.png)

Após isto clique em "Save" para salvar sua função lambda.

E agora está pronto, qual item que você enviar para o seu tópico SNS será encaminhado para seu grupo ou seu usuário no Telegram.